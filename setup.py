"""Setup file for "Pose Based Tactile Servoing for 2D Surfaces and Edges"
"""

from setuptools import setup

base_deps = ["numpy","pandas","matplotlib"]
model_deps = ["pose-models-2d"]

with open("README.md", 'r') as f:
    long_description = f.read()

setup(
    name="pose-based-tactile-servoing-2d",
    version="1.0",
    description="Pose Based Tactile Servoing (for) 2D (Surfaces and Edges)",
    license="GPLv3",
    long_description=long_description,
    author="Nathan Lepora",
    author_email="n.lepora@bristol.ac.uk",
    url="https://github.com/nlepora/",
    packages=["tactile_servoing_2d"],
    install_requires=[base_deps],
    extras_require={"model": model_deps},
)
